Nginx exporter role
=========

Installs Nginx exporter for Prometheus on Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.nginx_exporter
```
